import sys
import socket
import time
from random import randint
import copy
import numpy as np

t1 = 0.0  # the amount of time remaining to player 1
t2 = 0.0  # the amount of time remaining to player 2
MAX_DEPTH = 3
myPlayerNumber = -1
otherPlayerNumber = -1

state = [[0 for x in range(8)] for y in range(8)]  # state[0][0] is the bottom left corner of the board (on the GUI)

grid_weights = np.array([
    [3, 2, 2, 2, 2, 2, 2, 3],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 0, 0, 1, 1, 2],
    [2, 1, 1, 0, 0, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [2, 1, 1, 1, 1, 1, 1, 2],
    [3, 2, 2, 2, 2, 2, 2, 3],
])


### TODO Requirements for the basic lab
# First set a depth limit
# Then start exploring depth-first of the possible moves to the depth limit
# use the heuristic to evaluate the utility of that thing
#   What should we use? I say we use a simple heuristic at first to get it working (like number of pieces) and then we add a more creative heuristic for the creative part
# keep exploring using minimax, but prune with alpha-beta pruning (initialize alpha and beta)
# should we have a timer so it just picks something if it takes too long or should we just set the depht so it has time to calculate/prune everything?

# Then we have to do something "creative" what will it be?


# myPlayerNumber is a variable representing which player we are (black or white). It is either 1 or 2 and is used to determine lots of logic
# You should modify this function
# validMoves is a list of valid locations that you could place your "stone" on this turn
# Note that "state" is a global variable 2D list that shows the state of the game
# Return value for this function is the index in validMoves that we want to pick
# TODO we need alpha-beta pruning, minimax search, and a heuristic funciton to evealuate "Utility" of a secret
def move(round, validMoves):
    # if round % 10 == 0:
    #     return randint(0, len(validMoves) - 1)
    moveDict = {}
    # check move on all states
    for move in validMoves:
        # dictionary of heuristic utilities to moves
        moveDict[checkMove(round + 1, False, newStateFromMove(state, move, myPlayerNumber), 0)] = move
        # moveDict[checkMove(round , True, newStateFromMove(state, move, myPlayerNumber), 0)] = move
    # pick highest
    return validMoves.index(moveDict[max(moveDict, key=int)])

def possibleMovesHeuristic(round, playerNumber, state):
    # Get the number of moves available
    #def getValidMoves(round, playerNumber, currentState=None):
    return len(getValidMoves(round, playerNumber, state))

def gridHeuristic(state):
    # replace everything with the other player number with -1
    # replace everything with our player number with 1 
    modified_state = copy.deepcopy(state)
    for row in range(8):
        for element in range(8):
            #TODO try just having claimed spaces be 1 or -1 as well
            if state[row][element] == myPlayerNumber:
                modified_state[row][element] = 1
            elif state[row][element] == otherPlayerNumber:
                modified_state[row][element] = -1

    # multiply each matrix element wise and sum the results
    modified_state = np.array(modified_state)
    # printState(state)
    # print("&&&&&&&&&&&&&&&&")
    # print(np.multiply(grid_weights, modified_state))
    result = np.sum(np.multiply(grid_weights, modified_state))
    # print("Sum of grid weights: {}".format(np.sum(grid_weights)))
    # print("Result for grid heuristic: {}".format(result))
    return result

def numStonesHeuristic(state):
    # global thing
    # thing += 1
    # return thing
    count_player_1 = 0
    count_player_2 = 0
    for row in state:
        for square in row:
            if square == 1:
                count_player_1 += 1
            elif square == 2:
                count_player_2 += 1
    if myPlayerNumber == 1:
        return count_player_1 - count_player_2
    else:
        return count_player_2 - count_player_1

def comboHeuristic(round, state):
    return (gridHeuristic(state) * ((64 - round) / 64)) + ((safeStoneHeuristic(state) * (2**round)) / (2**64))

def checkToEnd(state, startx, starty, incx, incy):
    currentStone = state[startx][starty]
    currx = startx
    curry = starty

    while not ((curry < 0) or (curry > 7) or (currx < 0) or (currx > 7)):
        if state[currx][curry] != currentStone:
            return False
        currx = currx + incx
        curry = curry + incy
    return True

def safeStoneHeuristic(state):
    mySafeStoneCount = 0
    otherSafeStoneCount = 0
    # SAFE STONES: for any given stone, make sure that one side of each of the 4 directions is 
    # connected directly to an edge by your own stones
    # Check directions
    for y in range(8):
        for x in range(8):
            n = False
            ne = False
            e = False
            se = False
            s = False
            sw = False
            w = False
            nw = False
            
            if state[x][y] != 0:
                # Check east    
                e = checkToEnd(state, x, y, 1, 0)                                                                                                                                
                # Check west
                w = checkToEnd(state, x, y, -1, 0)
                # Check north
                n = checkToEnd(state, x, y, 0, 1)
                # Check south  
                s = checkToEnd(state, x, y, 0, -1)                      
                # Check North East
                ne = checkToEnd(state, x, y, 1, 1)
                # Check North West
                nw = checkToEnd(state, x, y, -1, 1)                
                # Check South East
                se = checkToEnd(state, x, y, 1, -1)
                # Check South West
                sw = checkToEnd(state, x, y, -1, -1)

            # Check opposite directions
            if (n or s) and (e or w) and (ne or sw) and (se or nw):
                if state[x][y] == myPlayerNumber:
                    # print("stone at ({},{}) is safe for me".format(x, y))
                    mySafeStoneCount += 1
                else:
                    # print("stone at ({},{}) is safe for my opponent".format(x, y))
                    otherSafeStoneCount += 1

    # print("in the end, i have {} safe stone(s) and my opponent has {}".format(mySafeStoneCount, otherSafeStoneCount))
    return mySafeStoneCount - otherSafeStoneCount


def masterHeuristic(round, playerNumber, state):
    # return numStonesHeuristic(state)
    # return gridHeuristic(state)
    # return possibleMovesHeuristic(round, playerNumber, state)
    # return comboHeuristic(round, state)
    return safeStoneHeuristic(state)

def printState(state):
    for i in reversed(range(8)):
        # pass
        print(state[i])

# Recursive method for checking the move
# Current player is the person whos turn it is, my player is the player represented by our code
def checkMove(round, isMyTurn, currentState, depth, alpha=float("-inf"), beta=float("inf")):
    # print("===== checkMove() called =====")
    currentPlayer = myPlayerNumber if isMyTurn else otherPlayerNumber
    validMoves = getValidMoves(round, currentPlayer, currentState)
    if len(validMoves) == 0 or depth == MAX_DEPTH:
        # leaf node
        # print("leaf")
        # printState(currentState)
        return masterHeuristic(round, currentPlayer, currentState)
    else:
        # not leaf node
        # print("Depth: {}, validMoves: {}".format(depth, validMoves))
        if isMyTurn:  # Maximize
            bestVal = float("-inf")
            for possibleMove in validMoves:
                moveVal = checkMove(round + 1, False, newStateFromMove(currentState, possibleMove, currentPlayer), depth + 1, alpha, beta)
                bestVal = max(moveVal, bestVal)
                alpha = max(alpha, bestVal)
                # TODO format print statements like a tree
                # print("possibleMove: {}, isMyTurn: {}, moveVal: {}, bestVal: {}, alpha: {}, beta: {}".format(possibleMove, isMyTurn, moveVal, bestVal, alpha, beta))
                if beta <= alpha:
                    # printState(currentState)
                    # print("Maximizer pruned at move {}".format(possibleMove))
                    break
            return bestVal
        else:  # Minimize
            bestVal = float("inf")
            for possibleMove in validMoves:
                moveVal = checkMove(round + 1, True, newStateFromMove(currentState, possibleMove, currentPlayer), depth + 1, alpha, beta)
                bestVal = min(bestVal, moveVal)
                beta = min(beta, bestVal)
                # print("possibleMove: {}, isMyTurn: {}, moveVal: {}, bestVal: {}, alpha: {}, beta: {}".format(possibleMove, isMyTurn, moveVal, bestVal, alpha, beta))
                if beta <= alpha:
                    # printState(currentState)
                    # print("Minimizer pruned at move {}".format(possibleMove))
                    break
            return bestVal

# establishes a connection with the server
def initClient(myPlayerNumber, thehost):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = (thehost, 3333 + myPlayerNumber)
    print('starting up on %s port %s' % server_address, file=sys.stderr)
    sock.connect(server_address)

    info = sock.recv(1024)

    print(info)

    return sock


# reads messages from the server
def readMessage(sock):
    message = sock.recv(1024).decode("utf-8").split("\n")
    # print(message)

    turn = int(message[0])
    # print("Turn: " + str(turn))

    if (turn == -999):
        time.sleep(1)
        sys.exit()

    round = int(message[1])
    # print("Round: " + str(round))
    # t1 = float(message[2])  # update of the amount of time available to player 1
    # print t1
    # t2 = float(message[3])  # update of the amount of time available to player 2
    # print t2

    count = 4
    for i in range(8):
        for j in range(8):
            state[i][j] = int(message[count])
            count = count + 1
    # printState(state)

    return turn, round

# As far as I can tell, this checks if you have a sequence of the other players stones starting from (row, col) (not counted) to the end of the board
# Correction, this checks if you can capture some stones starting at your stone at row, col and in the direction indicated by incx and incy
# incx and incy I think should be -1 or 1 to go  up or down. myPlayerNumber is which player you are
def checkDirection(row, col, incx, incy, myPlayerNumber, currentState=None):
    sequence = []
    # if we don't pass in a state, use the global current game state as the currentState
    if currentState == None:
        currentState = state
    for i in range(1, 8):
        r = row + incy * i
        c = col + incx * i

        if ((r < 0) or (r > 7) or (c < 0) or (c > 7)):
            break

        sequence.append(currentState[r][c])

    count = 0 # Honestly this could be a boolean and it would work too...
    for i in range(len(sequence)):
        if (myPlayerNumber == 1):
            if (sequence[i] == 2):
                count = count + 1
            else:
                if ((sequence[i] == 1) and (count > 0)):
                    return True
                break
        else:
            if (sequence[i] == 1):
                count = count + 1
            else:
                if ((sequence[i] == 2) and (count > 0)):
                    return True
                break

    return False

# WARNING, be sure to call check direction before you call this
def captureDirection(row, col, incx, incy, playerNumber, currentState=None):
    # if we don't pass in a state, use the global current game state as the currentState
    if currentState == None:
        currentState = state

    for i in range(1, 8):
        r = row + incy * i
        c = col + incx * i

        if currentState[r][c] == playerNumber:
            # if we hit another one of ours, then we are done
            return currentState
        else:
            # capture this stone
            currentState[r][c] = playerNumber

# This function checks if a specific location is a valid move for player `playerNumber`
# WARNING as far as I can tell, this doesn't check if there is already a piece at this location...
def couldBe(row, col, playerNumber, currentState=None):
    for incx in range(-1, 2):
        for incy in range(-1, 2):
            if ((incx == 0) and (incy == 0)):
                continue

            # Only place this is called so far
            if (checkDirection(row, col, incx, incy, playerNumber, currentState)):
                return True

    return False


# On the board matrix, 0 is empty, 1 is black and 2 is white
# generates the set of valid moves for the player; returns a list of valid moves (validMoves)
def getValidMoves(round, playerNumber, currentState=None):
    validMoves = []
    if currentState == None:
        currentState = state
    # print("Round: " + str(round))

    #for i in range(8):
    #    print(currentState[i])

    # This generates the valid moves for the first four moves, so we don't have to worry about programming that into the strategy
    # so round is a variable apparently that starts at 0 and incremements EVERY turn not every set of turns...
    # Come to think of it turn is probably just a 1 or 2 to indicate which player the turn currently is
    if (round < 4):
        if (currentState[3][3] == 0):
            validMoves.append([3, 3])
        if (currentState[3][4] == 0):
            validMoves.append([3, 4])
        if (currentState[4][3] == 0):
            validMoves.append([4, 3])
        if (currentState[4][4] == 0):
            validMoves.append([4, 4])
    else:
        for i in range(8):
            for j in range(8):
                # Ah, here it checks if the location is unoccupied, then it checks if it is valid for this player to move there
                if (currentState[i][j] == 0):
                    if (couldBe(i, j, playerNumber, currentState)):
                        validMoves.append([i, j])

    return validMoves

# TODO yeah so newStateFromMove doesn't actually capture pieces yet
def newStateFromMove(currentState, move, curPlayerNumber):
    if currentState[move[0]][move[1]] != 0:
        # print("THAT IS AN INVALID MOVE")
        # beware maybe this will cause an infinite loop
        return currentState
    # printState(currentState)
    # print("-----------------------------")
    newState = copy.deepcopy(currentState)
    newState[move[0]][move[1]] = curPlayerNumber
    # TODO check each direction
    for incx in range(-1, 2):
        for incy in range(-1, 2):
            if ((incx == 0) and (incy == 0)):
                continue

            # If we can capture in a direction, do it
            if (checkDirection(move[0], move[1], incx, incy, curPlayerNumber, newState)):
                newState = captureDirection(move[0], move[1], incx, incy, curPlayerNumber, newState)
    # printState(newState)
    return newState

# main function that (1) establishes a connection with the server, and then plays whenever it is this player's turn
# noinspection PyTypeChecker
def playGame(playerNumber, thehost):
    # create a random number generator

    sock = initClient(playerNumber, thehost)
    global myPlayerNumber
    global otherPlayerNumber

    myPlayerNumber = playerNumber
    if myPlayerNumber == 1:
        otherPlayerNumber = 2
    else:
        otherPlayerNumber = 1    

    while (True):
        print("Read")
        status = readMessage(sock)
        print("\n\nBoard for this move:")
        printState(state)
        print("\n\n")

        if (status[0] == myPlayerNumber):
            # print("Move-----------------------------------------------------")
            validMoves = getValidMoves(status[1], myPlayerNumber)
            # print(validMoves)

            myMove = move(status[1], validMoves)

            sel = str(validMoves[myMove][0]) + "\n" + str(validMoves[myMove][1]) + "\n"
            print("<" + sel + ">")
            sock.send(sel.encode("utf-8"))
            print("sent the message")
        else:
            print("It isn't my turn")


# call: python RandomGuy.py [ipaddress] [player_number]
# ipaddress is the ipaddress on the computer the server was launched on.  Enter "localhost" if it is on the same computer
# player_number is 1 (for the black player) and 2 (for the white player)
if __name__ == "__main__":
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))

    print(str(sys.argv[1]))

    playGame(int(sys.argv[2]), sys.argv[1])
