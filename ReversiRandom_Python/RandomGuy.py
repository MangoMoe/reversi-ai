import sys
import socket
import time
from random import randint

t1 = 0.0  # the amount of time remaining to player 1
t2 = 0.0  # the amount of time remaining to player 2

state = [[0 for x in range(8)] for y in range(8)]  # state[0][0] is the bottom left corner of the board (on the GUI)

# me is a variable representing which player we are (black or white). It is either 1 or 2 and is used to determine lots of logic
# You should modify this function
# validMoves is a list of valid locations that you could place your "stone" on this turn
# Note that "state" is a global variable 2D list that shows the state of the game
# Return value for this function is the index in validMoves that we want to pick
# TODO we need alpha-beta pruning, minimax search, and a heuristic funciton to evealuate "Utility" of a secret
def move(validMoves):
    # just return a random move
    myMove = randint(0, len(validMoves) - 1)

    # Requirements for the basic lab
    # First set a depth limit
    # Then start exploring depth-first of the possible moves to the depth limit
    # use the heuristic to evaluate the utility of that thing
    #   What should we use? I say we use a simple heuristic at first to get it working (like number of pieces) and then we add a more creative heuristic for the creative part
    # keep exploring using minimax, but prune with alpha-beta pruning (initialize alpha and beta)
    # should we have a timer so it just picks something if it takes too long or should we just set the depht so it has time to calculate/prune everything?

    # Then we have to do something "creative" what will it be?

    return myMove


# establishes a connection with the server
def initClient(me, thehost):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = (thehost, 3333 + me)
    print('starting up on %s port %s' % server_address, file=sys.stderr)
    sock.connect(server_address)

    info = sock.recv(1024)

    print(info)

    return sock


# reads messages from the server
def readMessage(sock):
    message = sock.recv(1024).decode("utf-8").split("\n")
    # print(message)

    turn = int(message[0])
    print("Turn: " + str(turn))

    if (turn == -999):
        time.sleep(1)
        sys.exit()

    round = int(message[1])
    print("Round: " + str(round))
    # t1 = float(message[2])  # update of the amount of time available to player 1
    # print t1
    # t2 = float(message[3])  # update of the amount of time available to player 2
    # print t2

    count = 4
    for i in range(8):
        for j in range(8):
            state[i][j] = int(message[count])
            count = count + 1
        print(state[i])

    return turn, round

# As far as I can tell, this checks if you have a sequence of the other players stones starting from (row, col) (not counted) to the end of the board
# Correction, this checks if you can capture some stones starting at your stone at row, col and in the direction indicated by incx and incy
# incx and incy I think should be -1 or 1 to go  up or down. Me is which player you are
def checkDirection(row, col, incx, incy, me):
    sequence = []
    for i in range(1, 8):
        r = row + incy * i
        c = col + incx * i

        if ((r < 0) or (r > 7) or (c < 0) or (c > 7)):
            break

        sequence.append(state[r][c])

    count = 0 # Honestly this could be a boolean and it would work too...
    for i in range(len(sequence)):
        if (me == 1):
            if (sequence[i] == 2):
                count = count + 1
            else:
                if ((sequence[i] == 1) and (count > 0)):
                    return True
                break
        else:
            if (sequence[i] == 1):
                count = count + 1
            else:
                if ((sequence[i] == 2) and (count > 0)):
                    return True
                break

    return False

# This function checks if a specific location is a valid move for player `me`
# WARNING as far as I can tell, this doesn't check if there is already a piece at this location...
def couldBe(row, col, me):
    for incx in range(-1, 2):
        for incy in range(-1, 2):
            if ((incx == 0) and (incy == 0)):
                continue

            # Only place this is called so far
            if (checkDirection(row, col, incx, incy, me)):
                return True

    return False


# On the board matrix, 0 is empty, 1 is black and 2 is white
# generates the set of valid moves for the player; returns a list of valid moves (validMoves)
def getValidMoves(round, me):
    validMoves = []
    print("Round: " + str(round))

    for i in range(8):
        print(state[i])

    # This generates the valid moves for the first four moves, so we don't have to worry about programming that into the strategy
    # so round is a variable apparently that starts at 0 and incremements EVERY turn not every set of turns...
    # Come to think of it turn is probably just a 1 or 2 to indicate which player the turn currently is
    if (round < 4):
        if (state[3][3] == 0):
            validMoves.append([3, 3])
        if (state[3][4] == 0):
            validMoves.append([3, 4])
        if (state[4][3] == 0):
            validMoves.append([4, 3])
        if (state[4][4] == 0):
            validMoves.append([4, 4])
    else:
        for i in range(8):
            for j in range(8):
                # Ah, here it checks if the location is unoccupied, then it checks if it is valid for this player to move there
                if (state[i][j] == 0):
                    if (couldBe(i, j, me)):
                        validMoves.append([i, j])

    return validMoves


# main function that (1) establishes a connection with the server, and then plays whenever it is this player's turn
# noinspection PyTypeChecker
def playGame(me, thehost):
    # create a random number generator

    sock = initClient(me, thehost)

    while (True):
        print("Read")
        status = readMessage(sock)

        if (status[0] == me):
            print("Move")
            validMoves = getValidMoves(status[1], me)
            print(validMoves)

            myMove = move(validMoves)

            sel = str(validMoves[myMove][0]) + "\n" + str(validMoves[myMove][1]) + "\n"
            print("<" + sel + ">")
            sock.send(sel.encode("utf-8"))
            print("sent the message")
        else:
            print("It isn't my turn")


# call: python RandomGuy.py [ipaddress] [player_number]
# ipaddress is the ipaddress on the computer the server was launched on.  Enter "localhost" if it is on the same computer
# player_number is 1 (for the black player) and 2 (for the white player)
if __name__ == "__main__":
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))

    print(str(sys.argv[1]))

    playGame(int(sys.argv[2]), sys.argv[1])
